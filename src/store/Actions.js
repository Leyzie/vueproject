checkState(context) {
  // здоровье
  if (this.store.health <= 0) {
    context.commit("updateState", {
      type: "dizableEat",
      value: true
    });
    context.commit("updateState", {
      type: "dizableDrink",
      value: true
    });
    context.commit("updateState", {
      type: "dizableRelax",
      value: true
    });
    context.commit("updateState", {
      type: "dizableWork",
      value: true
    });
    context.commit("updateState", {
      type: "error",
      value: "Конец игры! Ваш персонаж умер \u{1F480}"
    });
  }
  // жажда () писть
  if (this.store.thirst <= 0) {
    context.commit("updateState", {
      type: "dizableDrink",
      value: true
    });
  } else {
    context.commit("updateState", {
      type: "dizableDrink",
      value: false
    });
  }
  // Есть (голод)
  if (this.store.hunger <= 0) {
    context.commit("updateState", {
      type: "dizableRelax",
      value: true
    });
  } else {
    context.commit("updateState", {
      type: "dizableRelax",
      value: false
    });
  }
  // усталость (Работать)
  if (this.store.fatigue <= 0) {
    context.commit("updateState", {
      type: "dizableWork",
      value: true
    });
  } else {
    context.commit("updateState", {
      type: "dizableWork",
      value: false
    });
  }
},
