import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
//  https://habr.com/ru/post/421551/
// Пить (-10 жажды -1-(-1) hp)
// Есть (-10 голода -2-(-2) hp)
// Играть/работать (+30-40 жажды, +10-20 голода)
// Заняться спортом (+1-10 hp,  -x% голода/жажды)
/**
 * Массив с едой arrayFoot
 * 1 - Торт
 * 2 - Вафли
 * 3 - Яблоко
 * 4 - Хлеб
 * 5 - Брокли
 */
/**
 * Массив с едой arrayWater
 * 1 - Молоко
 * 2 - Вино
 * 3 - Вода
 */
function randomInteger(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

export default new Vuex.Store({
  state: {
    health: 50, // здоровье
    fatigue: 50, // усталость
    hunger: 50, // голод
    thirst: 50, // жажда
    histori: [], // История
    message: "", //Сообщение
    arrayFoot: [
      "\u{1F382}",
      "\u{1F95E}",
      "\u{1F34E}",
      "\u{1F35E}",
      "\u{1F966}"
    ],
    arrayWater: ["\u{1F95B}", "\u{1F377}", "\u{1F943}"],
    error: "",
    dizableEat: false,
    dizableDrink: false,
    dizableRelax: false,
    dizableWork: false,
    command: ["eat", "drink", "relax", "work", "help"],
    show: false
  },
  mutations: {
    updateState(state, { type, value }) {
      state[type] = value;
    },
    addHistory(state, value) {
      state.histori.push(value);
    }
  },
  actions: {
    isOpenMess(context) {
      context.commit("updateState", {
        type: "show",
        value: true
      });
      setTimeout(() => {
        context.commit("updateState", {
          type: "show",
          value: false
        });
      }, 10000);
    },
    terminalComand(context, value) {
      let command = this.state.command;
      let varindex = command.indexOf(value, 0);
      let result = "";
      switch (command[varindex]) {
        case "eat":
          this.dispatch("toEat");
          result = "Команда терминала eat";
          break;
        case "drink":
          this.dispatch("toDrink");
          result = "Команда терминала drink";
          break;
        case "relax":
          this.dispatch("toRelax");
          result = "Команда терминала relax";
          break;
        case "work":
          this.dispatch("toWork");
          result = "Команда терминала work";
          break;
        case "help":
          result =
            "Список команд: <br/> eat - Есть <br/> drink - Пить <br/> relax - Отдых <br/> work - Работать <br/>";
          break;
        default:
          result = "Для получения списка команд воспользуйтесь командой help";
          break;
      }
      console.log(result);
      context.commit("updateState", {
        type: "message",
        value: result
      });
      context.commit("addHistory", this.state.message);
    },
    checkState(context) {
      // здоровье
      // console.log("Здоровье " + this.state.health);
      // console.log("усталость " + this.state.fatigue);
      // console.log("голод " + this.state.hunger);
      // console.log("жажда " + this.state.thirst);
      if (this.state.health <= 0) {
        context.commit("updateState", {
          type: "dizableEat",
          value: true
        });
        context.commit("updateState", {
          type: "dizableDrink",
          value: true
        });
        context.commit("updateState", {
          type: "dizableRelax",
          value: true
        });
        context.commit("updateState", {
          type: "dizableWork",
          value: true
        });
        context.commit("updateState", {
          type: "error",
          value: "Конец игры! Ваш персонаж умер \u{1F480}"
        });
        context.commit("addHistory", this.state.message);
        return;
      }
      // жажда () писть
      // Есть (голод)
      if (this.state.hunger <= 101) {
        context.commit("updateState", {
          type: "dizableEat",
          value: false
        });
      } else {
        context.commit("updateState", {
          type: "dizableEat",
          value: true
        });
      }
      // усталость (Работать)
      if (this.state.fatigue <= 20) {
        context.commit("updateState", {
          type: "dizableWork",
          value: true
        });
      } else {
        context.commit("updateState", {
          type: "dizableWork",
          value: false
        });
      }
    },

    // Есть
    toEat(context) {
      this.dispatch("checkState");
      let random_boolean = Math.random() >= 0.8;
      if (random_boolean) {
        context.commit("updateState", {
          type: "health",
          value: this.state.health - 10
        });
        context.commit("updateState", {
          type: "message",
          value: "Вы отравились \u{1F691}"
        });
        context.commit("addHistory", this.state.message);
        return;
      } else {
        if (this.state.hunger >= 100) {
          context.commit("updateState", {
            type: "hunger",
            value: 100
          });
          context.commit("updateState", {
            type: "dizableEat",
            value: true
          });
          context.commit("updateState", {
            type: "message",
            value: `Вы сыты и не можете больше есть`
          });
          context.commit("addHistory", this.state.message);
          return;
        } else {
          context.commit("updateState", {
            type: "dizableEat",
            value: false
          });
          context.commit("updateState", {
            type: "health",
            value: this.state.health + 5
          });
          context.commit("updateState", {
            type: "hunger",
            value: this.state.hunger + 10
          });
          if (this.state.hunger >= 100) {
            context.commit("updateState", {
              type: "hunger",
              value: 100
            });
            context.commit("updateState", {
              type: "dizableEat",
              value: true
            });
            context.commit("updateState", {
              type: "message",
              value: `Вы сыты и не можете больше есть`
            });
            context.commit("addHistory", this.state.message);
            return;
          }
          var footRandom = randomInteger(0, 4);
          context.commit("updateState", {
            type: "message",
            value: `Вы съели  ${this.state.arrayFoot[footRandom]}`
          });
          context.commit("addHistory", this.state.message);
          return;
        }
      }
    },
    // Пить
    toDrink(context) {
      this.dispatch("checkState");
      if (this.state.thirst >= 100) {
        context.commit("updateState", {
          type: "thirst",
          value: 100
        });
        context.commit("updateState", {
          type: "dizableDrink",
          value: true
        });
        context.commit("updateState", {
          type: "message",
          value: `Ваша жажда уталина`
        });
        context.commit("addHistory", this.state.message);
        return;
      } else {
        context.commit("updateState", {
          type: "dizableDrink",
          value: false
        });
      }
      var drinkRandom = randomInteger(0, 2);
      switch (drinkRandom) {
        case 0:
          context.commit("updateState", {
            type: "message",
            value: `Вы выпили  ${this.state.arrayWater[drinkRandom]}`
          });
          context.commit("updateState", {
            type: "thirst",
            value: this.state.thirst + 10
          });
          if (this.state.thirst >= 100) {
            context.commit("updateState", {
              type: "thirst",
              value: 100
            });
            context.commit("updateState", {
              type: "dizableDrink",
              value: true
            });
            context.commit("updateState", {
              type: "message",
              value: `Ваша жажда уталина`
            });
            context.commit("addHistory", this.state.message);
            return;
          }
          context.commit("addHistory", this.state.message);
          break;
        case 1:
          context.commit("updateState", {
            type: "message",
            value: `Вы выпили алкоголь  ${this.state.arrayWater[drinkRandom]}`
          });
          context.commit("updateState", {
            type: "health",
            value: this.state.health - 2
          });
          context.commit("addHistory", this.state.message);
          break;
        case 2:
          context.commit("updateState", {
            type: "message",
            value: `Вы выпили  ${this.state.arrayWater[drinkRandom]}`
          });
          context.commit("updateState", {
            type: "thirst",
            value: this.state.thirst + 10
          });
          if (this.state.thirst >= 100) {
            context.commit("updateState", {
              type: "thirst",
              value: 100
            });
            context.commit("updateState", {
              type: "dizableDrink",
              value: true
            });
            context.commit("updateState", {
              type: "message",
              value: `Ваша жажда уталина`
            });
            context.commit("addHistory", this.state.message);
            return;
          }
          context.commit("addHistory", this.state.message);
          break;
      }
    },
    // Раслабиться (отдохнуть)
    toRelax(context) {
      this.dispatch("checkState");
      if (this.state.fatigue >= 100) {
        context.commit("updateState", {
          type: "fatigue",
          value: 100
        });
        context.commit("updateState", {
          type: "dizableRelax",
          value: true
        });
        context.commit("updateState", {
          type: "message",
          value: `Ваша Отдохнули полностью`
        });
        context.commit("addHistory", this.state.message);
        return;
      } else {
        context.commit("updateState", {
          type: "dizableRelax",
          value: false
        });
      }
      context.commit("updateState", {
        type: "fatigue",
        value: this.state.fatigue + 10
      });
      context.commit("updateState", {
        type: "hunger",
        value: this.state.hunger - 5
      });
      context.commit("updateState", {
        type: "hunger",
        value: this.state.thirst - 1
      });
      if (this.state.fatigue >= 100) {
        context.commit("updateState", {
          type: "fatigue",
          value: 100
        });
        context.commit("updateState", {
          type: "dizableRelax",
          value: true
        });
        context.commit("updateState", {
          type: "message",
          value: `Ваша Отдохнули полностью`
        });
        context.commit("addHistory", this.state.message);
        return;
      }
      context.commit("updateState", {
        type: "message",
        value: `Вы отдыхаете \u{26F1}`
      });
      context.commit("addHistory", this.state.message);
    },
    // Работать
    toWork(context) {
      this.dispatch("checkState");
      if (
        this.state.fatigue <= 20 ||
        this.state.hunger <= 20 ||
        this.state.thirst <= 20
      ) {
        context.commit("updateState", {
          type: "dizableWork",
          value: true
        });
        context.commit("updateState", {
          type: "message",
          value: `Вы переработали сверьте ваши параметры`
        });
        context.commit("addHistory", this.state.message);
        return;
      } else {
        context.commit("updateState", {
          type: "dizableWork",
          value: false
        });
      }
      context.commit("updateState", {
        type: "fatigue",
        value: this.state.fatigue - 10
      });
      context.commit("updateState", {
        type: "hunger",
        value: this.state.hunger - 2
      });
      context.commit("updateState", {
        type: "thirst",
        value: this.state.thirst - 1
      });
      if (
        this.state.fatigue >= 20 &&
        this.state.hunger <= 20 &&
        this.state.thirst <= 20
      ) {
        context.commit("updateState", {
          type: "dizableWork",
          value: true
        });
        context.commit("updateState", {
          type: "message",
          value: `Вы переработали сверьте ваши параметры`
        });
        context.commit("addHistory", this.state.message);
        return;
      }
      context.commit("updateState", {
        type: "message",
        value: `Вы работаете \u{1F477}`
      });
      context.commit("addHistory", this.state.message);
    }
  },
  modules: {},
  getters: {
    getUserInfo: state => state
  }
});
